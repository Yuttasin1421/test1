from django.urls import path
from .views import index,info
app_name='app1'

urlpatterns=[
    path('',index,name='index'),
    path('info/',info,name='info')
]