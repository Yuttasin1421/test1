from django.shortcuts import render
from .models import Student
# Create your views here.
def index(request):
    return render(request,'app1/index.html')

def info(request):
    students = Student.objects.all()
    context ={
        'students' : students
    }
    return render(request,'app1/info.html',context)

