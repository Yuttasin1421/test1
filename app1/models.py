from django.db import models
# Create your models here.
class Student(models.Model):
    name=models.CharField(max_length=99)
    f_name = models.CharField(max_length=99)
    l_name = models.CharField(max_length=99)
    dob = models.DateField()
